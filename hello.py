from flask import Flask, jsonify

app = Flask(__name__)

# Example code with potential security vulnerabilities (for demonstration)
@app.route('/')
def hello_world():
    # Insecure direct user input usage (replace with proper validation)
    user_input = request.args.get('name')
    return jsonify({'message': f'Hello, {user_input}!'})

if __name__ == '__main__':
    app.run(debug=True)
