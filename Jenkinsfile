pipeline {
    agent any

    stages {
        stage('Build Application') {
            steps {
                sh 'pip install Flask' // Install dependencies (assuming pip is installed)
                sh 'docker build -t test-app .' // Build the Docker image
            }
        }
        stage('Run SonarQube Analysis') {
            environment {
                // Replace with your SonarQube server details
                SONARQUBE_SERVER_URL = 'http://your-sonar-server:9000'
                SONAR_PROJECT_KEY = 'test-app'
                SONAR_SOURCES = '.' // Replace with your source code directory if needed
                SONAR_SCANNER_HOME = tool 'SonarQubeScanner'
            }
            steps {
                script {
                    def scannerHome = "${env.SONAR_SCANNER_HOME}"
                    sh """
                        ${scannerHome}/bin/sonar-scanner \
                        -Dsonar.projectKey=${env.SONAR_PROJECT_KEY} \
                        -Dsonar.sources=${env.SONAR_SOURCES} \
                        -Dsonar.host.url=${env.SONARQUBE_SERVER_URL}
                    """
                }
            }
        }
        stage('Run Trivy Scan') {
            steps {
                sh 'docker run --rm quay.io/vulnerabilityregistry/trivy test-app:latest'
            }
        }
        stage('Deploy (Optional)') { // Add this stage if you want to automate deployment
            steps {
                sh 'docker run -d --name test-app test-app:latest' // Run the container (replace with your deployment command)
            }
        }
    }
}
